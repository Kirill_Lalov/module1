﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            var module1 = new Module1();
            module1.SwapItems(5,6);
            module1.GetMinimumValue(new int[] { 22, 18, 17, 16 });
        }

        public int[] SwapItems(int a, int b)
        {
            int[] array = { b, a };
            for (var i = 0; i < array.Length; i++) 
            {
                Console.WriteLine("Your array element is: {0}", array[i]);
            }
            return array;
        }

        public int GetMinimumValue(int[] input)
        {
            int[] array = input;
            int minimumvalue = array[0];
            for (var i = 0; i < array.Length; i++)
            {
                if (minimumvalue > array[i])
                {
                    minimumvalue = array[i];
                }
                else
                {
                    minimumvalue = array[0];
                }
            }
            Console.WriteLine("The minimum value in the array is: {0}", minimumvalue);
            return minimumvalue;
        }
    }
}
